#include <signal.h>
#include <pthread.h>
#include <stdlib.h>
#include "ftp.h"
#include "ftp_debug.h"

pthread_t ftp_thread_id;
sigset_t signal_set;

// Signal handling thread
void* main_sig_thread(void* unused) {
  int sig;

  //if (sigfillset(&signal_set) == -1) {
  //  ftp_die("sigfillset");
  //}

  for ( ; ; ) {
    if (sigwait(&signal_set, &sig) != 0) {
      ftp_die("sigwait");
    }
    if (sig == SIGINT) {
      //if (pthread_detach(ftp_thread_id) != 0) {
      //  ftp_die("pthread_detach");
      //}
      if (pthread_cancel(ftp_thread_id) != 0) {
        ftp_die("pthread_cancel");
      }
      break;
    }
    else {
      ftp_log("catch unexpected signal = %d", sig);
    }
  }

  return NULL;
}


// The main program must take the signal processing.
//Server thread blocks all signals and all functions
//are written without signal interruption handling.

int main(int argc, char *argv[]) {
  pthread_t sig_thread_id;
  pthread_attr_t ftp_attr;
  pthread_attr_t sig_attr;

  // Block all signals, other threads created by main()
  //will inherit a copy of the signal mask.
  if (sigfillset(&signal_set) == -1) {
    ftp_die("sigfillset");
  }
  if (pthread_sigmask(SIG_BLOCK, &signal_set, NULL) != 0) {
    ftp_die("pthread_sigmask");
  }

  // Create server thread
  if (pthread_attr_init(&ftp_attr) != 0) {
    ftp_die("pthread_attr_init");
  }
  if (pthread_attr_setdetachstate(&ftp_attr, PTHREAD_CREATE_JOINABLE) != 0) {
    ftp_die("pthread_attr_setdetachstate");
  }
  if (pthread_create(&ftp_thread_id, &ftp_attr, main_ftp_thread, NULL) != 0) {
    ftp_die("pthread_create");
  }
  if (pthread_attr_destroy(&ftp_attr) != 0) {
    ftp_die("pthread_attr_destroy");
  }

  // Create thread for waiting signals
  if (pthread_attr_init(&sig_attr) != 0) {
    ftp_die("pthread_attr_init");
  }
  if (pthread_attr_setdetachstate(&sig_attr, PTHREAD_CREATE_DETACHED) != 0) {
    ftp_die("pthread_attr_setdetachstate");
  }
  if (pthread_create(&sig_thread_id, &sig_attr, main_sig_thread, NULL) != 0) {
    ftp_die("pthread_create");
  }
  if (pthread_attr_destroy(&sig_attr) != 0) {
    ftp_die("pthread_attr_destroy");
  }


  if (pthread_join(ftp_thread_id, NULL) != 0){
    ftp_die("pthread_join");
  }

  return 0;
}
