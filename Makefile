PROGRAM = dummyftp
CC      = gcc
CFLAGS  =
COMPILE = $(CC) -Wall $(CFLAGS) -c
LINK    = $(CC) $(CFLAGS)
LIBS    = -lpthread
PACKAGE = $(PROGRAM)
VERSION = 0.4pre
TARNAME = $(PACKAGE)
DISTDIR = $(PACKAGE)-$(VERSION)

all: $(PROGRAM)

debug: CFLAGS += -DFTP_DEBUG -g
debug: all

$(PROGRAM): ftp.o main.o ftp_debug.o
	$(LINK) ftp.o main.o ftp_debug.o -o $(PROGRAM) $(LIBS)
main.o: main.c ftp.h ftp_debug.h
	$(COMPILE) main.c
ftp.o: ftp.c ftp.h ftp_debug.h
	$(COMPILE) ftp.c
ftp_debug.o: ftp_debug.c ftp_debug.h ftp.h
	$(COMPILE) ftp_debug.c
clean:
	rm -rf *.o $(PROGRAM)

dist: $(DISTDIR).tar.bz2

$(DISTDIR).tar.bz2: FORCE $(DISTDIR)
	tar cof - $(DISTDIR) | bzip2 -c >$(DISTDIR).tar.bz2
	rm -rf $(DISTDIR)

$(DISTDIR):
	mkdir -p $(DISTDIR)
	cp Makefile $(DISTDIR)
	cp README $(DISTDIR)
	cp README.ru $(DISTDIR)
	cp ftp.c $(DISTDIR)
	cp ftp.h $(DISTDIR)
	cp ftp_debug.c $(DISTDIR)
	cp ftp_debug.h $(DISTDIR)
	cp main.c $(DISTDIR)

FORCE:
	-rm $(DISTDIR).tar.bz2 &> /dev/null
	-rm -rf $(DISTDIR) &> /dev/null

.PHONY: FORCE all clean dist
