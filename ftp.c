#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>
#include <ctype.h>
#include <pthread.h>
#include <errno.h>
#include "ftp.h"
#include "ftp_debug.h"

// limits /////////////
#define MAX_CLIENTS       1
#define COMMAND_LINE_SIZE 1024
#define COMMAND_SIZE      32
#define COMMAND_ARG_SIZE  1024
#define USERNAME_SIZE     32
#define PASSWORD_SIZE     32
#define FILE_STAT_STRSIZE 1024
#define FILE_MODE         0666
#define DIR_MODE          0777
#define CONNECT_TIMEOUT   10
#define SOCKIO_TIMEOUT    10
#define RESPONSE_SIZE     1024
#define READBUFF_SIZE     1024
#define PREFIX_SIZE       1024
#define FTP_PATH_MAX      1024
///////////////////////

#define bool unsigned int
#define TRUE  1
#define FALSE 0

// command codes
enum {
  NEXT,
  RECV_ERROR,
  RECV_TIMEOUT,
  COM_USER,
  COM_PASS,
  COM_CWD,
  COM_PWD,
  COM_PORT,
  COM_TYPE,
  COM_LIST,
  COM_QUIT,
  COM_RETR,
  COM_STOR,
  COM_RMD,
  COM_MKD,
  COM_DELE,
  COM_UNKNOWN
};

// responses
#define R150_STATUSOK       150
#define R200_COMMANDOK      200
#define R220_HELLO          220
#define R221_GOODBYE        221
#define R226_TRANSFEROK     226
#define R230_LOGINOK        230
#define R250_FOPERATIONOK   250
#define R257_PWDOK          257
#define R331_SPECIFYPASS    331
#define R425_CANTOPEN       425
#define R426_CONNCLOSED     426
#define R451_LOCALERROR     451
#define R501_SYNTAXERROR    501
#define R502_NOTIMPLEMENTED 502
#define R503_USERFIRST      503
#define R530_LOGININCORRECT 530
#define R550_FOPERATIONFAIL 550


typedef struct session {
  int  client_fd;
  int  data_sock;
  int  data_fd;
  char line_buff[COMMAND_LINE_SIZE];
  char command_buff[COMMAND_SIZE];
  char command_arg[COMMAND_ARG_SIZE];
  struct sockaddr_in data_connection_addr;
  int  is_binary;
  char username[USERNAME_SIZE];
  char password[PASSWORD_SIZE];
  char file_stat_str[FILE_STAT_STRSIZE];
  char response_buff[RESPONSE_SIZE];
  char read_buff[READBUFF_SIZE];
  char prefix[PREFIX_SIZE];
} session;

static pthread_key_t session_key;


static void init_session(session* client_session) {
  client_session->client_fd = -1;
  client_session->data_sock = -1;
  client_session->data_fd = -1;
  client_session->line_buff[0] = '\0';
  client_session->command_buff[0] = '\0';
  client_session->command_arg[0] = '\0';
  client_session->is_binary = TRUE;
  client_session->username[0] = '\0';
  client_session->password[0] = '\0';
  client_session->file_stat_str[0] = '\0';
  client_session->response_buff[0] = '\0';
  client_session->prefix[0] = '/';
  client_session->prefix[1] = '\0';
}

static void close_session_descriptors(session* client_session) {
  close(client_session->client_fd);
  close(client_session->data_sock);
  close(client_session->data_fd);
}

static void free_session(void* sp) {
  if (sp != NULL) {
    session *ss = sp;
    close_session_descriptors(ss);
    free(ss);
  }
}

static void  client_handling(session* client_session);
static int   send_response(session* client_session, unsigned int status, char* msg);
static int   wait_command(session* client_session);
static void  get_username(session* client_session);
static void  get_password(session* client_session);
static int   set_file_stat(session* client_session, char* fname);
static int   send_list(session* client_session, const char* path);
static int   send_file(session* client_session);
static int   download_file(session* client_session);
static int   get_data_connection(session* client_session);
static int   change_directory(session* client_session);
static int   send_prefix(session* client_session);
static int   make_directory(session* client_session);
static int   remove_directory(session* client_session);
static int   remove_file(session* client_session);
static char* ftp_path_resolution(session* client_session, const char *fpath, char *rpath);
/////////////////////////////////////////////////////////////////////////
static void  activate_noblock(int fd);
static void  deactivate_noblock(int fd);
static int   connect_timeout(int fd, const struct sockaddr* addr, socklen_t addrlen, unsigned int wait_seconds);
static void  fdclose(int* fd);
static void  set_socket_iotimeout(int sockfd, int timeout_sec);
static size_t bsd_strlcat(char *dst, const char *src, size_t siz);
static size_t bsd_strlcpy(char *dst, const char *src, size_t siz);
static char*  bsd_basename_r(const char *path, char *bname);
static char*  bsd_dirname_r(const char *path, char *dname);
static char*  bsd_realpath(session* client_session, const char *path, char *resolved);



void* main_ftp_thread(void* unused) {
  int server_sock;
  struct sockaddr_in server_addr;
  session* client_session = NULL;
  session* old_client_session = NULL;
  int client_fd;

  server_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (server_sock == -1) {
    ftp_die("server_sock = socket(...)");
  }

  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(FTP_PORT);
  if (inet_aton(FTP_IP, &server_addr.sin_addr) == 0) {
    close(server_sock);
    ftp_die("inet_aton()");
  }

  if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
    close(server_sock);
    ftp_die("bind(server_sock, ...)");
  }

  if (listen(server_sock, MAX_CLIENTS) == -1) {
    close(server_sock);
    ftp_die("listen(server_sock, ...)");
  }

  if (pthread_key_create(&session_key, free_session) != 0) {
    close(server_sock);
    ftp_die("pthread_key_create");
  }

  for ( ; ; ) {
    client_fd = accept(server_sock, NULL, NULL);
    if (client_fd == -1) {
      ftp_log("CLIENT DISCONNECTED");
      continue;
    }

    old_client_session = pthread_getspecific(session_key);
    client_session = malloc(sizeof(session));
    if (client_session == NULL) {
      ftp_perror("malloc(...)");
      close(client_fd);
      continue;
    }
    free_session(old_client_session);
    if (pthread_setspecific(session_key, client_session) != 0) {
      free_session(client_session);
      close(client_fd);
      ftp_die("pthread_setsecific");
    }
    init_session(client_session);
    client_session->client_fd = client_fd;

    client_handling(client_session);
  }

  // never reach
  return NULL;
}


static void client_handling(session* client_session) {
  int resp;
  int com_code;
  bool user_logged = FALSE;
  int retval;
  char* cur_char;

  // set session timeout
  set_socket_iotimeout(client_session->client_fd, FTP_SESSION_TIMEOUT);

  ftp_log("CONNECT: Client");
  resp = send_response(client_session, R220_HELLO, "(dummyftp)");
  if (resp == -1) {
    goto exit;
  }

  for ( ; ; ) {
    com_code = wait_command(client_session);

    switch (com_code) {
    case COM_USER:
      if (user_logged) {
        resp = send_response(client_session, R530_LOGININCORRECT, "Can't change to another user.");
        if (resp == -1) {
          goto exit;
        }
        continue;
      }
      get_username(client_session);
      resp = send_response(client_session, R331_SPECIFYPASS, "Please specify the password.");
      if (resp == -1) {
        goto exit;
      }
      com_code = wait_command(client_session);
      if (com_code != COM_PASS) {
        ftp_log("FAIL LOGIN");
        send_response(client_session, R530_LOGININCORRECT, "Login incorrect.");
        send_response(client_session, R221_GOODBYE, "Goodbye.");
        goto exit;
      }
      get_password(client_session);
      if ((strncmp(client_session->username, FTP_USER, sizeof(FTP_USER)) != 0) &&
          (strncmp(client_session->password, FTP_PASS, sizeof(FTP_PASS)) != 0)) {
        ftp_log("FAIL LOGIN");
        send_response(client_session, R530_LOGININCORRECT, "Login incorrect.");
        send_response(client_session, R221_GOODBYE, "Goodbye.");
        goto exit;
      }
      ftp_log("LOGIN OK");
      resp = send_response(client_session, R230_LOGINOK, "Login successful.");
      if (resp == -1) {
        goto exit;
      }
      user_logged = TRUE;
      break;
    case COM_PASS:
      if (user_logged) {
        resp = send_response(client_session, R230_LOGINOK, "Already logged in.");
      }
      else {
        resp = send_response(client_session, R503_USERFIRST, "Login with USER first.");
      }
      if (resp == -1) {
        goto exit;
      }
      continue;
      break;
    case COM_TYPE:
      resp = send_response(client_session, R200_COMMANDOK, "Command successful.");
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_PWD:
      resp = send_prefix(client_session);
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_CWD:
      retval = change_directory(client_session);
      if (retval == 0) {
        resp = send_response(client_session, R250_FOPERATIONOK, "File operation successfully completed.");
      }
      else {
        resp = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_PORT:
      if (get_data_connection(client_session) == 0) {
        resp = send_response(client_session, R200_COMMANDOK, "Command successful.");
      }
      else {
        resp = send_response(client_session, R501_SYNTAXERROR, "Syntax error in parameters or arguments.");
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_LIST:
      // find path if presented
      if (client_session->command_arg[0] == '-') {
        for (cur_char = &client_session->command_arg[1]; *cur_char != '\0' && *cur_char != ' '; ++cur_char) {
          ;
        }
        if (*cur_char != '\0') {
          ++cur_char;
          retval = send_list(client_session, cur_char);
        }
        else {
          retval = send_list(client_session, "./");
        }
      }
      else if (client_session->command_arg[0] != '\0') {
        retval = send_list(client_session, client_session->command_arg);
      }
      else {
        retval = send_list(client_session, "./");
      }

      if (retval == 0) {
        resp = send_response(client_session, R226_TRANSFEROK, "Transer successfully completed.");
      }
      else {
        switch(retval) {
        case R425_CANTOPEN:
          resp = send_response(client_session, R425_CANTOPEN, "Can't open data connection.");
          break;
        case R426_CONNCLOSED:
          resp = send_response(client_session, R426_CONNCLOSED, "Connection closed; transfer aborted.");
          break;
        case R451_LOCALERROR:
          resp = send_response(client_session, R451_LOCALERROR, "Requested action aborted. Local error in processing.");
          break;
        default:
          ftp_perror("internal error!");
        };
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_RETR:
      retval = send_file(client_session);
      if (retval == -1) {
        goto exit;
      }
      break;
    case COM_STOR:
      //if (send_response(client_session, R150_STATUSOK, "File status okay; about to open data connection.") == -1) {
      //  goto exit;
      //}
      retval = download_file(client_session);
      if (retval == 0) {
        resp = send_response(client_session, R226_TRANSFEROK, "Transer successfully completed.");
      }
      else {
        switch(retval) {
        case R425_CANTOPEN:
          resp = send_response(client_session, R425_CANTOPEN, "Can't open data connection.");
          break;
        case R426_CONNCLOSED:
          resp = send_response(client_session, R426_CONNCLOSED, "Connection closed; transfer aborted.");
          break;
        case R451_LOCALERROR:
          resp = send_response(client_session, R451_LOCALERROR, "Requested action aborted. Local error in processing.");
          break;
        default:
          ftp_perror("internal error!");
        };
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_RMD:
      retval = remove_directory(client_session);
      if (retval == 0) {
        resp = send_response(client_session, R250_FOPERATIONOK, "File operation successfully completed.");
      }
      else {
        resp = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_MKD:
      retval = make_directory(client_session);
      if (retval == 0) {
        resp = send_response(client_session, R250_FOPERATIONOK, "File operation successfully completed.");
      }
      else {
        resp = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_DELE:
      retval = remove_file(client_session);
      if (retval == 0) {
        resp = send_response(client_session, R250_FOPERATIONOK, "File operation successfully completed.");
      }
      else {
        resp = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
      }
      if (resp == -1) {
        goto exit;
      }
      break;
    case COM_QUIT:
      send_response(client_session, R221_GOODBYE, "Goodbye.");
      goto exit;
      break;
    case COM_UNKNOWN:
      resp = send_response(client_session, R502_NOTIMPLEMENTED, "Command not implemented.");
      if (resp == -1) {
        goto exit;
      }
      break;
    case NEXT:
      break;
    case RECV_ERROR:
      goto exit;
      break;
    case RECV_TIMEOUT:
      send_response(client_session, R221_GOODBYE, "Goodbye.");
      goto exit;
      break;
    default:
      ftp_perror("unexpected behavior");
      goto exit;
    }
  }
exit:
  return;
}


static int send_response(session* client_session, unsigned int status, char* msg) {
  int retval = 0;
  int bytes = 0;
  bytes = snprintf(client_session->response_buff, sizeof(client_session->response_buff), "%u %s\r\n", status, msg);
  if (bytes == sizeof(client_session->response_buff)) {
    ftp_perror("response message was truncated");
  }
  ftp_log("FTP response: \"%.*s\"", strlen(client_session->response_buff) - 2, client_session->response_buff);
  if (send(client_session->client_fd, client_session->response_buff, strlen(client_session->response_buff), 0) == -1) {
    ftp_log("CLIENT DISCONNECTED");
    retval = -1;
  }
  return retval;
}


static int wait_command(session* client_session) {
  int com_code;
  int rec_len;
  unsigned int i;
  char* cur_char;
  char* schar;

  // peek to socket
  memset(client_session->line_buff, 0, sizeof(client_session->line_buff));
  rec_len = recv(client_session->client_fd, client_session->line_buff, sizeof(client_session->line_buff), MSG_PEEK);
  if (rec_len && (errno == EAGAIN || errno == EWOULDBLOCK || errno == ETIMEDOUT)) {
    ftp_log("CLIENT TIMEOUT");
    return RECV_TIMEOUT;
  }
  if (rec_len == -1) {
    ftp_perror("recv(client_fd, ...)");
    return RECV_ERROR;
  }
  cur_char = client_session->line_buff;
  for (i = 0; i < rec_len; ++i) {
    if (client_session->line_buff[i] == '\n') {
      recv(client_session->client_fd, client_session->line_buff, i + 1, 0);
      cur_char = strtok_r(cur_char, " \r", &schar);
      if (cur_char == NULL) {
        ftp_perror("strtok_r(...)");
        return RECV_ERROR;
      }
      strncpy(client_session->command_buff, cur_char, sizeof(client_session->command_buff));
      client_session->command_buff[sizeof(client_session->command_buff) - 1] = '\0';
      cur_char = strtok_r(NULL, "\r", &schar);
      if ((cur_char == NULL) || (*cur_char == '\n')) {
        client_session->command_arg[0] = '\0';
      }
      else {
        strncpy(client_session->command_arg, cur_char, sizeof(client_session->command_arg));
        client_session->command_arg[sizeof(client_session->command_arg) - 1] = '\0';
      }
    }
    if (i == rec_len) {
      return NEXT;
    }
  }

  if (strncmp("USER", client_session->command_buff, sizeof("USER") - 1) == 0) {
    com_code = COM_USER;
  }
  else if (strncmp("PASS", client_session->command_buff, sizeof("PASS") - 1) == 0) {
    com_code = COM_PASS;
  }
  else if (strncmp("QUIT", client_session->command_buff, sizeof("QUIT") - 1) == 0) {
    com_code = COM_QUIT;
  }
  else if (strncmp("PORT", client_session->command_buff, sizeof("PORT") - 1) == 0) {
    com_code = COM_PORT;
  }
  else if (strncmp("LIST", client_session->command_buff, sizeof("LIST") - 1) == 0) {
    com_code = COM_LIST;
  }
  else if (strncmp("CWD", client_session->command_buff, sizeof("CWD") - 1) == 0) {
    com_code = COM_CWD;
  }
  else if (strncmp("TYPE", client_session->command_buff, sizeof("TYPE") - 1) == 0) {
    com_code = COM_TYPE;
  }
  else if (strncmp("RETR", client_session->command_buff, sizeof("RETR") - 1) == 0) {
    com_code = COM_RETR;
  }
  else if (strncmp("STOR", client_session->command_buff, sizeof("STOR") - 1) == 0) {
    com_code = COM_STOR;
  }
  else if (strncmp("RMD", client_session->command_buff, sizeof("RMD") - 1) == 0) {
    com_code = COM_RMD;
  }
  else if (strncmp("MKD", client_session->command_buff, sizeof("MKD") - 1) == 0) {
    com_code = COM_MKD;
  }
  else if (strncmp("DELE", client_session->command_buff, sizeof("DELE") - 1) == 0) {
    com_code = COM_DELE;
  }
  else if (strncmp("PWD", client_session->command_buff, sizeof("PWD") - 1) == 0) {
    com_code = COM_PWD;
  }
  else {
    com_code = COM_UNKNOWN;
  }
  ftp_log("FTP command: \"%s %s\"", client_session->command_buff, client_session->command_arg);
  return com_code;
}

static void get_username(session* client_session) {
  strncpy(client_session->username, client_session->command_arg, sizeof(client_session->username));
  client_session->username[sizeof(client_session->username) - 1] = '\0';
}
static void get_password(session* client_session) {
  strncpy(client_session->password, client_session->command_arg, sizeof(client_session->password));
  client_session->password[sizeof(client_session->password) - 1] = '\0';
}
static int get_data_connection(session* client_session) {
  char* cur_char;
  char* commap;

  cur_char = client_session->command_arg;
  memset(&client_session->data_connection_addr, 0, sizeof(client_session->data_connection_addr));
  client_session->data_connection_addr.sin_family = AF_INET;

  commap = strchr(cur_char, ',');
  if (commap == NULL) return - 1;
  *commap = '.';
  commap = strchr(cur_char, ',');
  if (commap == NULL) return - 1;
  *commap = '.';
  commap = strchr(cur_char, ',');
  if (commap == NULL) return - 1;
  *commap = '.';
  commap = strchr(cur_char, ',');
  if (commap == NULL) return - 1;
  *commap = '\0';
  ++commap;

  if (inet_aton(cur_char, &client_session->data_connection_addr.sin_addr) == 0) {
    ftp_perror("inet_aton()");
    return -1;
  }

  cur_char = commap;
  commap = strchr(cur_char, ',');
  if (commap == NULL) return - 1;
  *commap = '\0';
  ++commap;

  client_session->data_connection_addr.sin_port = htons(atoi(cur_char) * 256 + atoi(commap));

  return 0;
}
static int send_prefix(session* client_session) {
  char pwd_response[FTP_PATH_MAX];
  char* c = NULL;
  int cb = 0;

  pwd_response[cb++] = '"';
  c = client_session->prefix;
  while (*c != '\0') {
    // change \012 to \000
    if (*c == '\012') {
      pwd_response[cb++] = '\000';
    }
    // change " to ""
    else if (*c == '"') {
      pwd_response[cb++] = '"';
      pwd_response[cb++] = '"';
    }
    else {
      pwd_response[cb++] = *c;
    }
    // check scope
    if (cb >= FTP_PATH_MAX-2) { // -2 for last " and \0
      return send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
    }
    ++c;
  }
  pwd_response[cb++] = '"';
  pwd_response[cb] = '\0';
  return send_response(client_session, R257_PWDOK, pwd_response);
}
static int change_directory(session* client_session) {
  DIR* d;
  struct dirent entry;
  struct dirent* result_p;
  char new_prefix[FTP_PATH_MAX];
  char* res;
  int cb;
  int rdir_result;

  res = bsd_realpath(client_session, client_session->command_arg, new_prefix);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    return -1;
  }
  // check directory
  d = opendir(new_prefix);
  if (d == NULL) {
    return -1;
  }
  rdir_result = readdir_r(d, &entry, &result_p);
  closedir(d);
  if (rdir_result != 0) {
    return -1;
  }
  client_session->prefix[0] = '\0';
  cb = bsd_strlcpy(client_session->prefix, new_prefix, PREFIX_SIZE);
  if (cb >= PREFIX_SIZE) {
    ftp_perror("bsd_strlcpy");
    return -1;
  }

  return 0;
}
static int make_directory(session* client_session) {
  char path[FTP_PATH_MAX] = {0};
  char *res;

  res = ftp_path_resolution(client_session, client_session->command_arg, path);
  if (res == NULL) {
    ftp_perror("ftp_path_resolution");
    return -1;
  }

  if (mkdir(path, DIR_MODE) == -1) {
    ftp_perror("mkdir");
    return -1;
  }

  return 0;
}
static int remove_directory(session* client_session) {
  char path[FTP_PATH_MAX];
  char *res;

  res = bsd_realpath(client_session, client_session->command_arg, path);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    return -1;
  }

  if (rmdir(path) == -1) {
    ftp_perror("rmdir");
    return -1;
  }

  return 0;
}
static int remove_file(session* client_session) {
  char path[FTP_PATH_MAX];
  char *res;

  res = bsd_realpath(client_session, client_session->command_arg, path);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    return -1;
  }

  if (remove(path) == -1) {
    ftp_perror("remove");
    return -1;
  }

  return 0;
}


static int send_list(session* client_session, const char* lpath) {
  struct dirent entry;
  struct dirent* result_p;
  DIR *d = NULL;
  struct stat sb;
  int retval = 0;
  char path[FTP_PATH_MAX];
  char raw_path[FTP_PATH_MAX];
  int cb;
  char* res;

  res = bsd_realpath(client_session, lpath, path);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    retval = R451_LOCALERROR;
    goto exit;
  }

  if (stat(path, &sb) == -1) {
    ftp_perror("stat path");
    retval = R451_LOCALERROR;
    goto exit;
  }
  else {
    if (send_response(client_session, R150_STATUSOK, "File status okay; about to open data connection.") == -1) {
      goto exit;
    }
  }

  // establish data connection
  client_session->data_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (client_session->data_sock == -1) {
    ftp_perror("data_sock = socket(...)");
    retval = R425_CANTOPEN;
    goto exit;
  }
  // sock timeout
  set_socket_iotimeout(client_session->data_sock, SOCKIO_TIMEOUT);

  if (connect_timeout(client_session->data_sock, (struct sockaddr*)&client_session->data_connection_addr,
      sizeof(client_session->data_connection_addr), CONNECT_TIMEOUT) == -1) {
    ftp_perror("connect_timeout(data_sock, ...)");
    retval = R425_CANTOPEN;
    goto exit;
  }

  // if directory
  if ((sb.st_mode & S_IFMT) == S_IFDIR) {
    if (!(d = opendir(path))) {
      ftp_perror("opendir");
      retval = R451_LOCALERROR;
      goto exit;
    }
    while ((readdir_r(d, &entry, &result_p) == 0) && (result_p != NULL)) {
      cb = bsd_strlcpy(raw_path, path, FTP_PATH_MAX);
      if (cb >= FTP_PATH_MAX) {
        ftp_perror("bsd_strlcpy");
        goto exit;
      }
      if (raw_path[cb - 1] != '/') {
        bsd_strlcat(raw_path, "/", FTP_PATH_MAX);
      }
      cb = bsd_strlcat(raw_path, entry.d_name, FTP_PATH_MAX);
      if (cb >= FTP_PATH_MAX) {
        ftp_perror("bsd_strlcat");
        goto exit;
      }
      retval = set_file_stat(client_session, raw_path);
      if (retval != 0) {
        ftp_perror("set_file_stat");
        goto exit;
      }
      if (send(client_session->data_sock, client_session->file_stat_str, strlen(client_session->file_stat_str), 0) == -1) {
        ftp_perror("send");
        retval = R426_CONNCLOSED;
        goto exit;
      }
    }
  }
  else { // if not directory
    retval = set_file_stat(client_session, path);
    if (retval != 0) {
      ftp_perror("set_file_stat");
      goto exit;
    }
    if (send(client_session->data_sock, client_session->file_stat_str, strlen(client_session->file_stat_str), 0) == -1) {
      ftp_perror("send");
      retval = R426_CONNCLOSED;
      goto exit;
    }
  }

exit:
  if (d) {
    closedir(d);
  }
  if (client_session->data_sock != -1) {
    fdclose(&client_session->data_sock);
  }
  return retval;
}

static int send_file(session* client_session) {
  int retval = 0;
  int opts;
  int rsize;
  char path[FTP_PATH_MAX];
  char *res;

  res = bsd_realpath(client_session, client_session->command_arg, path);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    retval = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
    goto exit;
  }

  // open file
  client_session->data_fd = open(path, O_NONBLOCK | O_RDONLY);
  if (client_session->data_fd == -1) {
     retval = send_response(client_session, R550_FOPERATIONFAIL, "File operation failed.");
     goto exit;
  }

  if (send_response(client_session, R150_STATUSOK, "File status okay; about to open data connection.") == -1) {
    retval = -1;
    goto exit;
  }

  // establish data connection
  client_session->data_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (client_session->data_sock == -1) {
    ftp_perror("data_sock = socket(...)");
    retval = send_response(client_session, R425_CANTOPEN, "Can't open data connection.");
    goto exit;
  }
  // make transfer unbuffered
  opts = fcntl(client_session->data_sock, F_GETFL, 0);
  if (fcntl(client_session->data_sock, F_SETFL, opts | O_ASYNC) == -1) {
     ftp_perror("fcntl");
     retval = send_response(client_session, R451_LOCALERROR, "Requested action aborted. Local error in processing.");
     goto exit;
  }
  // sock timeout
  set_socket_iotimeout(client_session->data_sock, SOCKIO_TIMEOUT);

  if (connect_timeout(client_session->data_sock, (struct sockaddr*)&client_session->data_connection_addr,
      sizeof(client_session->data_connection_addr), CONNECT_TIMEOUT) == -1) {
    ftp_perror("connect_timeout(data_sock, ...)");
    retval = send_response(client_session, R425_CANTOPEN, "Can't open data connection.");
    goto exit;
  }

  while ((rsize = read(client_session->data_fd, client_session->read_buff, sizeof(READBUFF_SIZE)))) {
    if (send(client_session->data_sock, client_session->read_buff, rsize, 0) == -1) {
      ftp_perror("send");
      retval = send_response(client_session, R426_CONNCLOSED, "Connection closed; transfer aborted.");
      goto exit;
    }
  }
  retval = send_response(client_session, R226_TRANSFEROK, "Transer successfully completed.");

exit:
  if (client_session->data_fd != -1) {
    fdclose(&client_session->data_fd);
  }
  if (client_session->data_sock != -1) {
    fdclose(&client_session->data_sock);
  }
  return retval;
}


static int download_file(session* client_session) {
  int retval = 0;
  int opts;
  int rsize;
  char path[FTP_PATH_MAX] = {0};
  char *res;

  res = ftp_path_resolution(client_session, client_session->command_arg, path);
  if (res == NULL) {
    ftp_perror("path too long");
    retval = R451_LOCALERROR;
    goto exit;
  }

  // open file
  client_session->data_fd = open(path, O_NONBLOCK | O_WRONLY | O_CREAT | O_TRUNC, FILE_MODE);
  if (client_session->data_fd == -1) {
     ftp_perror("open file");
     retval = R451_LOCALERROR;
     goto exit;
  }
  else {
    if (send_response(client_session, R150_STATUSOK, "File status okay; about to open data connection.") == -1) {
      retval = R451_LOCALERROR;
      goto exit;
    }
  }

  // make write sync mode
  opts = fcntl(client_session->data_fd, F_GETFL, 0);
  if (fcntl(client_session->data_fd, F_SETFL, opts | O_SYNC) == -1) {
     ftp_perror("fcntl");
     retval = R451_LOCALERROR;
     goto exit;
  }

  // establish data connection
  client_session->data_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (client_session->data_sock == -1) {
    ftp_perror("data_sock = socket(...)");
    retval = R425_CANTOPEN;
    goto exit;
  }
  // make transfer unbuffered
  opts = fcntl(client_session->data_sock, F_GETFL, 0);
  if (fcntl(client_session->data_sock, F_SETFL, opts | O_ASYNC) == -1) {
     ftp_perror("fcntl");
     retval = R451_LOCALERROR;
     goto exit;
  }
  // sock timeout
  set_socket_iotimeout(client_session->data_sock, SOCKIO_TIMEOUT);

  if (connect_timeout(client_session->data_sock, (struct sockaddr*)&client_session->data_connection_addr,
      sizeof(client_session->data_connection_addr), CONNECT_TIMEOUT) == -1) {
    ftp_perror("connect_timeout(data_sock, ...)");
    retval = R425_CANTOPEN;
    goto exit;
  }

  while ((rsize = recv(client_session->data_sock, client_session->read_buff, READBUFF_SIZE, 0))) {
    if (rsize == -1) {
      ftp_perror("recv");
      retval = R426_CONNCLOSED;
      goto exit;
    }
    if (write(client_session->data_fd, client_session->read_buff, rsize) == -1) {
      ftp_perror("write");
      retval = R451_LOCALERROR;
      goto exit;
    }
  }

exit:
  if (client_session->data_fd != -1) {
    fdclose(&client_session->data_fd);
  }
  if (client_session->data_sock != -1) {
    fdclose(&client_session->data_sock);
  }
  return retval;
}


static int set_file_stat(session* client_session, char* fname) {
  struct stat sb;
  char fmode[11] = "----------";
  char date[16];
  char basename[FTP_PATH_MAX];

  if (stat(fname, &sb) == -1) {
    ftp_perror("stat path %s", fname);
    return R451_LOCALERROR;
  }

  if ((sb.st_mode & S_IFMT) == S_IFDIR) {
    fmode[0] = 'd';
  }
  else {
    fmode[0] = '-';
  }
  fmode[1] = (sb.st_mode & S_IRUSR) ? 'r' : '-';
  fmode[2] = (sb.st_mode & S_IWUSR) ? 'w' : '-';
  fmode[3] = (sb.st_mode & S_IXUSR) ? 'x' : '-';
  fmode[4] = (sb.st_mode & S_IRGRP) ? 'r' : '-';
  fmode[5] = (sb.st_mode & S_IWGRP) ? 'w' : '-';
  fmode[6] = (sb.st_mode & S_IXGRP) ? 'x' : '-';
  fmode[7] = (sb.st_mode & S_IROTH) ? 'r' : '-';
  fmode[8] = (sb.st_mode & S_IWOTH) ? 'w' : '-';
  fmode[9] = (sb.st_mode & S_IXOTH) ? 'x' : '-';

  if (strftime(date, sizeof(date), "%b %d %H:%M", localtime(&sb.st_mtime)) == 0) {
    ftp_perror("strftime");
    return R451_LOCALERROR;
  }

  // for os2000
  if (sb.st_nlink == 0) {
    sb.st_nlink = 1;
  }

  snprintf(client_session->file_stat_str, sizeof(client_session->file_stat_str), "%s%5ld%5d%9d%14lld%13s %s\r\n",
    fmode, sb.st_nlink, sb.st_uid, sb.st_gid,
    (long long)sb.st_size, date, bsd_basename_r(fname, basename));
  ftp_log("%*.*s", strlen(client_session->file_stat_str)-1, strlen(client_session->file_stat_str)-1, client_session->file_stat_str);
  return 0;
}

// DESCRIPTION
//   Function ftp_path_resolution() operates using bsd_realpath(..., path, ...) and
//   does the same thing. Function is applied when the path contains the name of the
//   file or directory at the end that is not presented in the file system. Because
//   bsd_realpath() returns an error if any of the components in the path is not
//   presented int the system.
// CAVEAT
//   resolved buffer must be equal to FTP_PATH_MAX
static char* ftp_path_resolution(session* client_session, const char *path, char *resolved) {
  char name[FTP_PATH_MAX] = {0};
  char *res = NULL;

  res = bsd_dirname_r(path, name);
  if (res == NULL) {
    ftp_perror("bsd_dirname_r");
    return NULL;
  }
  if (name[0] != '/') {
    (void)bsd_strlcpy(resolved, client_session->prefix, FTP_PATH_MAX);
  }
  (void)bsd_strlcat(resolved, "/", FTP_PATH_MAX);
  (void)bsd_strlcat(resolved, name, FTP_PATH_MAX);
  res = bsd_realpath(client_session, resolved, name);
  if (res == NULL) {
    ftp_perror("bsd_realpath");
    return NULL;
  }
  (void)bsd_strlcpy(resolved, name, FTP_PATH_MAX);
  (void)bsd_strlcat(resolved, "/", FTP_PATH_MAX);
  res = bsd_basename_r(path, name);
  if (res == NULL) {
    ftp_perror("bsd_basename_r");
    return NULL;
  }
  (void)bsd_strlcat(resolved, name, FTP_PATH_MAX);
  if (strlen(resolved) == FTP_PATH_MAX-1) {
    ftp_perror("path too long");
    return NULL;
  }

  return resolved;
}
///////////////////// system functions /////////////////////////
static void fdclose(int* fd) {
  if (close(*fd) == -1) {
    ftp_die("close");
  }
  *fd = -1;
}

static void set_socket_iotimeout(int sockfd, int timeout_sec) {
  struct timeval timeout;
  timeout.tv_sec = timeout_sec;
  timeout.tv_usec = 0;
  if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1) {
    ftp_die("setsockopt");
  }
  if (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) == -1) {
    ftp_die("setsockopt");
  }
}

static void activate_noblock(int fd) {
  int retval;
  int curr_flags = fcntl(fd, F_GETFL);
  if (curr_flags == -1) {
    ftp_die("fcntl");
  }
  curr_flags |= O_NONBLOCK;
  retval = fcntl(fd, F_SETFL, curr_flags);
  if (retval != 0) {
    ftp_die("fcntl");
  }
}

static void deactivate_noblock(int fd) {
  int retval;
  int curr_flags = fcntl(fd, F_GETFL);
  if (curr_flags == -1) {
    ftp_die("fcntl");
  }
  curr_flags &= ~O_NONBLOCK;
  retval = fcntl(fd, F_SETFL, curr_flags);
  if (retval != 0) {
    ftp_die("fcntl");
  }
}

static int connect_timeout(int fd, const struct sockaddr* addr, socklen_t addrlen, unsigned int wait_seconds) {
  int retval;
  fd_set connect_fdset;
  struct timeval timeout;
  int saved_errno;

  if (wait_seconds > 0) {
    activate_noblock(fd);
  }
  retval = connect(fd, addr, addrlen);
  if (retval < 0 && errno == EINPROGRESS) {
    FD_ZERO(&connect_fdset);
    FD_SET(fd, &connect_fdset);
    timeout.tv_sec = wait_seconds;
    timeout.tv_usec = 0;
    do {
      retval = select(fd + 1, NULL, &connect_fdset, NULL, &timeout);
      saved_errno = errno;
    } while (retval < 0 && saved_errno == EINTR);
    if (retval <= 0) {
      if (retval == 0) {
        errno = EAGAIN;
      }
      retval = -1;
    }
    else
    {
      socklen_t socklen = sizeof(retval);
      int sockoptret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &retval, &socklen);
      if (sockoptret != 0) {
        ftp_die("getsockopt");
      }
      if (retval != 0) {
        errno = retval;
        retval = -1;
      }
    }
  }
  if (wait_seconds > 0) {
    deactivate_noblock(fd);
  }
  return retval;
}


// DESCRIPTION
//   The bsd_strlcpy() and bsd_strlcat() functions copy and
//   concatenate strings respectively. They are designed to be safer, more
//   consistent, and less error prone replacements for strncpy(3) and strncat(3).
//   Unlike those functions, bsd_strlcpy() and bsd_strlcat() take the full size
//   of the buffer (not just the length) and guarantee to NUL-terminate the result
//   (as long as size is larger than 0 or, in the case of bsd_strlcat(), as long as
//   there is at least one byte free in dst). Note that a byte for the NUL should
//   be included in size/ Also note that bsd_strlcpy() and bsd_strlcat() only
//   operate on true "C" strings. This means that for bsd_strlcpy() src must be
//   NUL-terminated and for bsd_strlcat() both src and dst must be NUL-terminated.
//
//   The bsd_strlcpy() function copies up to size - 1 characters from the
//   NUL-terminated string src to dst, NUL-terminating the result.
//
//   The bsd_strlcat() function appends the NUL-terminated string src to the
//   end of dst. It will append at most size - strlen(dst) - 1 bytes, NUL-terminating
//   the result.
// RETURN VALUES
//   The bsd_strlcpy() and bsd_strlcat() functions return the total length of the
//   string they tried to create. For bsd_strlcpy() that means the length of src.
//   For bsd_strlcat() that means the initial length of dst plus the length of src.
//   While this may seem somewhat confusing, it was done to make truncation detection
//   simple.
// CAVEAT
//   The source and destination strings should not overlap, as the behavior is undefined.
size_t bsd_strlcat(char *dst, const char *src, size_t siz) {
  char *d = dst;
  const char *s = src;
  size_t n = siz;
  size_t dlen;

  // Find the end of dst and adjust bytes left but don't go past end
  while (n-- != 0 && *d != '\0') {
    d++;
  }
  dlen = d - dst;
  n = siz - dlen;

  if (n == 0) {
    return(dlen + strlen(s));
  }
  while (*s != '\0') {
    if (n != 1) {
      *d++ = *s;
      n--;
    }
    s++;
  }
  *d = '\0';

  return (dlen + (s - src)); // count does not include NUL 
}
// See description above
size_t bsd_strlcpy(char *dst, const char *src, size_t siz) {
  char *d = dst;
  const char *s = src;
  size_t n = siz;

  // Copy as many bytes as will fit
  if (n != 0) {
    while (--n != 0) {
      if ((*d++ = *s++) == 0) break;
    }
  }

  // Not enough room in dst, add NUL and traverse rest of src
  if (n == 0) {
    if (siz != 0) {
      *d = '\0'; // NUL-terminate dst
    }
    while (*s++) ;
  }

  return (s - src - 1); // count does not include NUL
}

// DESCRIPTION
//   The bsd_basename_r() function returns the last component from the pathname
//   pointed to by path, deleting any trailing '/' characters.
//   bname is a buffer of at least FTP_PATH_MAX bytes in which to store the resulting component.
//   If path consists entirely of '/' charecters, a pointer to the string "/" is returned. If path
//   is a null pointer or the empty string, a pointer to the string "." is returned.
// RETURN VALUES
//   On successful completion, bsd_basename_r() return pointer to the last component of path.
//   If the fail, a null pointer is returned.
// CAVEAT
//   bname buffer must be equal to FTP_PATH_MAX
char* bsd_basename_r(const char *path, char *bname)
{
  const char *endp;
  const char *startp;
  int cb;
  const size_t bsiz = FTP_PATH_MAX;

  if (bname == NULL) {
    return NULL;
  }

  // Empty or NULL string gets treated as "."
  if (path == NULL || *path == '\0') {
    cb = bsd_strlcpy(bname, ".", bsiz);
    if (cb >= bsiz) {
      return NULL;
    }
    return bname;
  }

  // Strip trailing slashes
  endp = path + strlen(path) - 1;
  while (endp > path && *endp == '/') {
    endp--;
  }

  // All slashes becomes "/"
  if (endp == path && *endp == '/') {
    cb = bsd_strlcpy(bname, "/", bsiz);
    if (cb >= bsiz) {
      return NULL;
    }
    return bname;
  }

  // Find the start of the base
  startp = endp;
  while (startp > path && *(startp - 1) != '/') {
    startp--;
  }

  if (endp - startp + 2 > bsiz) {
    return NULL;
  }
  (void)strncpy(bname, startp, endp - startp + 1);
  bname[endp - startp + 1] = '\0';

  return bname;
}


// DESCRIPTION
//   The bsd_dirname_r() function is the converse of bsd_basename_r(); it returns a pointer to
//   the parent directory of the pathname pointed to by path. Any trailing '/' charecters
//   are not counted as part of the directory name.
//   dname is a buffer of at least FTP_PATH_MAX bytes in which to store the resulting component.
//   If path is a null pointer, the empty
//   string, or contains no '/' charecters, bsd_dirname_r() returns a pointer to the string ".".
//   sygnifying the current directory.
// RETURN VALUES
//   On succrssful copmletion, bsd_dirname_r() returns a pointer to the parent directory of path.
//   If bsd_dirname_r() fails, a null pointer is returned.
// CAVEAT
//   dname buffer must be equal to FTP_PATH_MAX
char* bsd_dirname_r(const char *path, char *dname)
{
  size_t len;
  const char *endp;
  const size_t dsize = FTP_PATH_MAX;

  if (dname == NULL) {
    return NULL;
  }

  // Empty or NULL string gets treated as "."
  if (path == NULL || *path == '\0') {
    dname[0] = '.';
    dname[1] = '\0';
    return dname;
  }

  // Strip any trailing slashes
  endp = path + strlen(path) - 1;
  while (endp > path && *endp == '/') {
    endp--;
  }

  // Find the start of the dir
  while (endp > path && *endp != '/') {
    endp--;
  }

  // Either the dir is "/" or there are no slashes
  if (endp == path) {
    dname[0] = *endp == '/' ? '/' : '.';
    dname[1] = '\0';
    return dname;
  } else {
    // Move forward past the separating slashes
    do {
      endp--;
    } while (endp > path && *endp == '/');
  }

  len = endp - path + 1;
  if (len >= dsize) {
    return NULL;
  }
  memcpy(dname, path, len);
  dname[len] = '\0';
  return dname;
}


// Find the real name of path, by removing all ".", "..".
// Returns (resolved) on success, or (NULL) on failure,
// in which case the path which caused trouble is left in (resolved).
// WARNING: resolved buffer must be equal to FTP_PATH_MAX
char* bsd_realpath(session* client_session, const char *path, char *resolved) {
  struct stat sb;
  char *p, *q, *s;
  size_t left_len, resolved_len;
  char left[FTP_PATH_MAX], next_token[FTP_PATH_MAX];

  if (path == NULL) {
    return (NULL);
  }
  if (path[0] == '\0') {
    return (NULL);
  }
  if (resolved == NULL) {
    return (NULL);
  }

  if (path[0] == '/') {
    resolved[0] = '/';
    resolved[1] = '\0';
    if (path[1] == '\0') {
      return (resolved);
    }
    resolved_len = 1;
    left_len = bsd_strlcpy(left, path + 1, sizeof(left));
  }
  else {
    bsd_strlcpy(resolved, client_session->prefix, FTP_PATH_MAX);
    resolved_len = strlen(resolved);
    left_len = bsd_strlcpy(left, path, sizeof(left));
  }
  if (left_len >= sizeof(left) || resolved_len >= FTP_PATH_MAX) {
    return (NULL);
  }

  //
  // Iterate over path components in `left'.
  //
  while (left_len != 0){
    //
    // Extract the next path component and adjust `left'
    // and its length.
    //
    p = strchr (left, '/');
    s = p ? p : left + left_len;
    if (s - left >= sizeof(next_token)) {
      return (NULL);
    }
    memcpy (next_token, left, s - left);
    next_token[s - left] = '\0';
    left_len -= s - left;
    if (p != NULL) {
      memmove (left, s + 1, left_len + 1);
    }
    if (resolved[resolved_len - 1] != '/') {
      if (resolved_len + 1 >= FTP_PATH_MAX) {
        return (NULL);
      }
      resolved[resolved_len++] = '/';
      resolved[resolved_len] = '\0';
    }
    if (next_token[0] == '\0') {
      // Handle consequential slashes.  The path
      // before slash shall point to a directory.
      //
      // Only the trailing slashes are not covered
      // by other checks in the loop, but we verify
      // the prefix for any (rare) "//" or "/\0"
      // occurence to not implement lookahead.
      if (stat (resolved, &sb) != 0) {
        return (NULL);
      }
      if (!S_ISDIR (sb.st_mode)) {
        return (NULL);
      }
      continue;
    }
    else if (strcmp (next_token, ".") == 0) {
      continue;
    }
    else if (strcmp (next_token, "..") == 0) {
      // Strip the last path component except when we have
      // single "/"
      if (resolved_len > 1) {
        resolved[resolved_len - 1] = '\0';
        q = strrchr (resolved, '/') + 1;
        *q = '\0';
        resolved_len = q - resolved;
      }
      continue;
    }

    // Append the next path component and stat() it.
    resolved_len = bsd_strlcat(resolved, next_token, FTP_PATH_MAX);
    if (resolved_len >= FTP_PATH_MAX) {
      return (NULL);
    }
    if (stat (resolved, &sb) != 0) {
      return (NULL);
    }
  }

  // Remove trailing slash except when the resolved pathname
  // is a single "/".
  if (resolved_len > 1 && resolved[resolved_len - 1] == '/') {
    resolved[resolved_len - 1] = '\0';
  }
  return (resolved);
}
