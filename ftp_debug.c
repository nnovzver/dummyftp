#include <stdio.h>
#include <stdarg.h>
#include <pthread.h>

#include "ftp.h"
#include "ftp_debug.h"

//#define PRINTF_LIKE_DEBUG printf
#define STR_SIZE 1024

#if defined(FTP_LOG) || defined(FTP_EREG)
static pthread_mutex_t STR_mutex = PTHREAD_MUTEX_INITIALIZER;
static char STR[STR_SIZE];
#endif

void ftp_log(const char* s, ...) {
#ifdef FTP_LOG
    va_list va;
    if (pthread_mutex_lock(&STR_mutex) == 0) {
        pthread_cleanup_push(pthread_mutex_unlock, &STR_mutex);
        va_start(va, s);
        vsnprintf(STR, STR_SIZE, s, va);
        va_end(va);
        // handle STR
        PRINTF_LIKE("FTP_LOG: %s\n", STR);
        pthread_cleanup_pop(1);
    }
#endif
}

void ftp_perror(const char* s, ...) {
#ifdef FTP_EREG
    va_list va;
    if (pthread_mutex_lock(&STR_mutex) == 0) {
        pthread_cleanup_push(pthread_mutex_unlock, &STR_mutex);
        va_start(va, s);
        vsnprintf(STR, STR_SIZE, s, va);
        va_end(va);
        // handle STR
        PRINTF_LIKE("FTP_ERROR: %s\n", STR);
        pthread_cleanup_pop(1);
    }
#endif
}

void ftp_die(const char* s, ...) {
#ifdef FTP_EREG
    va_list va;
    if (pthread_mutex_lock(&STR_mutex) == 0) {
        pthread_cleanup_push(pthread_mutex_unlock, &STR_mutex);
        va_start(va, s);
        vsnprintf(STR, STR_SIZE, s, va);
        va_end(va);
        // handle STR
        PRINTF_LIKE("FTP_FATAL: %s\n", STR);
        pthread_cleanup_pop(1);
    }
#endif
  // cancel thread
  pthread_exit(NULL);
}
