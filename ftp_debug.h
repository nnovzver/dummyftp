#ifndef FTP_DEBUG_H
#define FTP_DEBUG_H

#ifdef __cplusplus
extern "C"
{
#endif

void ftp_log(const char* s, ...);
void ftp_perror(const char* s, ...);
void ftp_die(const char* s, ...);


#ifdef __cplusplus
}
#endif

#endif // FTP_DEBUG_H
