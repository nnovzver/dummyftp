#ifndef FTP_H
#define FTP_H

#ifdef __cplusplus
extern "C"
{
#endif

/* config */
#define FTP_PORT    8888
#define FTP_IP      "193.1.1.77"
#define FTP_USER    "user"
#define FTP_PASS    "pass"
#define FTP_SESSION_TIMEOUT   60 // if client does nothing session ends (in seconds)
#undef  FTP_DEBUG
#define PRINTF_LIKE printf // debugging function
#include <stdio.h>         // debugging function header

// debugging
#ifdef FTP_DEBUG
  #define  FTP_LOG     // define this if you want log operations
  #define  FTP_EREG    // define this if you want error registration
#endif

// thread function
void* main_ftp_thread(void*);

#ifdef __cplusplus
}
#endif

#endif // FTP_H
